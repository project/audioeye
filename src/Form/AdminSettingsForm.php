<?php

/**  
 * @file  
 * Contains Drupal\audioeye\Form\AdminSettingsForm.  
 */

namespace Drupal\audioeye\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class AdminSettingsForm extends ConfigFormBase
{
    const API_URL       = AUDIOEYE_API_URL;
    const API_INSTALL   = 'drupal/install';
    const API_SIGNATURE = 'drupal/signature';

    /**  
     * {@inheritdoc}  
     */
    protected function getEditableConfigNames()
    {
        return [
            'audioeye.admin_settings',
        ];
    }

    /**  
     * {@inheritdoc}  
     */
    public function getFormId()
    {
        return 'audioeye_admin_settings';
    }

    /**  
     * {@inheritdoc}  
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('audioeye.admin_settings');

        $dashboard_url = Url::fromRoute('audioeye.admin_dashboard');

        if ($this->isDisconnecting($form_state)) {
            $this->doApiDisconnect();
        }

        $form['audioeye_link_dashboard'] = [
            '#type'     => 'inline_template',
            '#template' => '<div>Navigate to  &raquo; <a style="{{style}}" href="{{url}}">Dashboard</a> <hr></div>',
            '#context'  => [
                'url'   => $dashboard_url,
                'style' => 'display: inline-block; margin: 15px 0',
            ],
        ];

        $form['settings_app_email'] = [
            '#type'          => 'email',
            '#title'         => $this->t('Account Email'),
            '#description'   => $this->t('Enter your the email address used when registering with AudioEye'),
            '#default_value' => $config->get('email'),
        ];

        $form['settings_site_url'] = [
            '#type'     => 'textfield',
            '#disabled' => true,
            '#title'    => $this->t('Base URL'),
            '#default_value' => \Drupal::request()->getSchemeAndHttpHost(),
        ];

        $form['settings_ext_id'] = [
            '#type'     => 'textfield',
            '#disabled' => true,
            '#title'    => $this->t('Extension ID'),
            '#default_value' => $config->get('ext_id'),
        ];

        $form['settings_site_hash'] = [
            '#type'          => 'textfield',
            '#disabled'      => true,
            '#title'         => $this->t('Site Hash'),
            '#default_value' => $config->get('site_hash'),
        ];

        $form['settings_signature'] = [
            '#type'          => 'textfield', 
            '#disabled'      => true,
            '#title'         => $this->t('Site Signature'),
            '#default_value' => $config->get('signature'),
        ];

        $form['settings_timestamp'] = [
            '#type'          => 'textfield',
            '#disabled'      => true,
            '#title'         => $this->t('Date Connected'),
            '#default_value' =>  $config->get('timestamp') ? date('Y-m-d H:i:s A', $config->get('timestamp')) . " ({$config->get('timestamp')})" : '',
            '#attributes'    => [
                'style' => 'margin: 0 0 15px 0'
            ]            
        ];

        $form['settings_auto_embed'] = [
            '#type'        => 'checkbox',

            '#title'       => $this->t('Automatic Script Injection'),
            '#description' => $this->t(
                'When set, the AudioEye Tag (embed code) will automatically be injected <br>'
                    . 'into your site footer. To manually insert the embed code, deselect this <br>'
                    . 'checkbox and retrieve the embed code from the <a href="@url">Dashboard</a>.',
                [
                    '@url' => $dashboard_url->toString(),
                ]
            ),
            '#checked' => $config->get('auto_embed') === 1,
            '#default_value' => $config->get('auto_embed')
        ];

                
        $form = parent::buildForm($form, $form_state);

        $form['actions']['submit']['#value'] = $this->t('Save');

        $form['actions']['disconnect'] = [
            '#type'     => 'button',
            '#disabled' => $this->isApiConnected() ? 0 : 1,
            '#name'     => 'disconnect',
            '#value'    => $this->t('Disconnect'),
            '#description' => $this->t('Welcome message display to users when they login'),
            '#attributes'  => [
                'style' => 'margin-top: 15px;margin-bottom: 15px'
            ]
        ];

        $form['actions']['status'] = [
            '#type'     => 'inline_template',
            '#template' => '<div class="button" style="{{ style }}">{{ status }}</div>',
            '#context'  => [
                'status'   => $this->isApiConnected() ? 'Active' : 'Inactive',
                'style' => implode(';', [
                    'background: #8BC34A',
                    'display: inline-block',
                    'text-shadow:none',
                    'color:white',
                    'border-color:#8BC34A',
                    'font-weight: inherit',
                    'margin-left: 0'
                ]),
            ],
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $email = $form_state->getValue('settings_app_email');
        
        if (!$this->isDisconnecting($form_state) && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $form_state->setErrorByName('admin_settings_app_email', 'Invalid email address, please try again.');
        }
    }

    /**  
     * {@inheritdoc}  
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitForm($form, $form_state);

        $email      = $form_state->getValue('settings_app_email');
        $auto_embed = $form_state->getValue('settings_auto_embed');

        $extension = $this->getExtensionDetails($email);

        $this->config('audioeye.admin_settings')
            ->set('email', $email)
            ->set('auto_embed', $auto_embed === 1 ? 1 : null)
            ->set('ext_id', $extension['ext_id'])
            ->set('site_hash', $extension['site_hash'])
            ->set('timestamp', $extension['timestamp'])
            ->set('signature', $extension['signature'])
            ->save();
    }

    /**
     * Get the API extension details for the given email from AudioEye.
     * 
     * This method gets the extension ID and then exchanges it for an API signature,
     * the results of which are merged an returned as an array for persisting to 
     * database.
     *
     * @return array
     */
    protected function getExtensionDetails(string $email)
    {
        $request   = $this->getApiExtensionId($email);
        $signature = $this->getApiSignature($request['ext_id']);

        return array_merge($request, $signature);

    }

    /**
     * Get the API URL given an optional endpoint.
     *
     * @param string $endpoint
     *
     * @return string
     */
    protected function getApiUrl(string $endpoint = '')
    {
        return trim(self::API_URL, "\/") . '/' . $endpoint;
    }

    /**
     * Get the API extension ID from AudioEye.
     *
     * @param string $endpoint
     *
     * @return array|null
     */
    protected function getApiExtensionId(string $email)
    {
        return $this->doApiRequest('POST', self::API_INSTALL, [
            'headers' => [
                'Accept' => 'application/json',
            ],
            'form_params' => [
                "email"    => $email,
                "base_url" => \Drupal::request()->getSchemeAndHttpHost()
            ],
        ]);
    }

    /**
     * Get the API signature from AudioEye.
     *
     * @param string $ext_id
     *
     * @return array|null
     */
    protected function getApiSignature(string $ext_id)
    {
        return $this->doApiRequest('GET', self::API_SIGNATURE, [
            'headers' => [
                'Accept' => 'application/json',
            ],
            'query' => [
                "ext_id" => $ext_id
            ],
        ]);
    }

    /**
     * Determine if the request is attempting to disconnect from the AudioEye API.
     *
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *
     * @return boolean
     */
    protected function isDisconnecting(FormStateInterface $form_state)
    {
        $trigger = $form_state->getTriggeringElement();
        return array_key_exists('#name', $trigger) && $trigger['#name'] === 'disconnect';        
    }

    /**
     * Determine whether the user account is connected to the AudioEye API.
     *
     * @todo improve this, at present we check for existence of non-empty values
     *       which can be replaced with a more concise check again the signature
     *       site_has and ext_id lengths. 
     * 
     * @return boolean
     */
    protected function isApiConnected()
    {
        $config = $this->config('audioeye.admin_settings');

        $data = $config->get('email') 
                && $config->get('signature') 
                && $config->get('ext_id') 
                && $config->get('site_hash')
                && $config->get('timestamp');

        return $data;
    }
    
    /**
     * Disconnect from the AudioAPI.
     *
     * @return boolean
     */
    protected function doApiDisconnect()
    {
        $config = $this->config('audioeye.admin_settings');
        $config->delete();

        return empty($config->getRawData());
    }

    /**
     * Given a method, endpoint and options, make a request to the AudioEye API return its response.
     *
     * @param string $method
     * @param string $endpoint
     * @param array $options
     *
     * @return array|null
     */
    protected function doApiRequest(string $method, string $endpoint = '', array $options = [])
    {
        try {
            /** @var GuzzleHttp\Client $client */
            $client = \Drupal::httpClient();

            $request = $client->request(
                $method,
                $this->getApiUrl($endpoint),
                $options
            );

            $response = $request->getBody()->getContents();

            if (is_string($response)) {
                $response = json_decode($response, JSON_OBJECT_AS_ARRAY);
            }

            return $response;
        } catch (\Exception $error) {

            $prefix = 'AUDIOEYE_ERROR_BEGIN::' . __FUNCTION__;
            $suffix = 'AUDIOEYE_ERROR_END::'  . __FUNCTION__;

            $message = [
                $prefix,
                "ERROR:\n\n {$error->getMessage()}",
                "TRACE:\n\n {$error->getTraceAsString()}",
                $suffix,
                str_repeat('#', 80)
            ];

            error_log(implode("\n\n", $message));

            \Drupal::logger('audioeye')->error(implode("\n", $message));
        }

        // if null we have a problem, check the logs
        // for debug info and any exceptions.
        return null;
    }
}
