<?php

namespace Drupal\audioeye\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Class AdminDashboardController.
 */
class AdminDashboardController extends ControllerBase
{
    const ACCOUNT_BASE_URL = 'https://portal.audioeye.com';

    /**
     * Return AudioEye iframe dashboard.
     *
     * @return array
     */
    public function index()
    {
        $config  = $this->config('audioeye.admin_settings');
        $page = [];

        $page['audioeye_link_dashboard'] = [
            '#type'     => 'inline_template',
            '#template' => '<div>Navigate to &raquo; <a style="{{ style }}" href="{{ url }}">Settings</a> <hr></div>',
            '#context'  => [
                'url'   => Url::fromRoute('audioeye.admin_settings'),
                'style' => 'display: inline-block; margin: 15px 0',
            ],
        ];

        $page['audioeye_admin_iframe'] = [
            '#type'     => 'inline_template',
            '#template' => '<iframe onload="{{ onload }}" frameborder="0" id="audioeye-dashboard" width="100%" src="{{ url | raw }}"></iframe>',
            '#context'  => [
                'url' => sprintf(
                    AUDIOEYE_ACCOUNT_URL . '?ext_id=%s&sig=%s&ts=%s&channel_name=drupal',
                    $config->get('ext_id'),
                    $config->get('signature'),
                    $config->get('timestamp')
                ),
                'onload' => '__AudioEyeDrupal.resize("#audioeye-dashboard")'
            ],
        ];

        return $page;
    }
}
