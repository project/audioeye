## Alpha Release
This module is under active development and does have a few kinks to work out. We anticipate a stable release very soon.

For now we recommend that Drupal site owners create an AudioEye account at https://www.audioeye.com and use the custom installation method to install your AudioEye Tag. Thank you for your understanding.

## AudioEye Package

This package is intended for Drupal 8+ only, for Drupal 7 and above please consult https://gitlab.com/bonafidetech/audioeye-drupal-7.

### Requirements

This package is compatible with:

- Drupal version `8.*`
- PHP version `>=5.5` (minimum)

We recommend:

- PHP version `>=7.2` (recommended) (as Drupal 8.5.0)

### Installation

Run `composer install`.

### Development

To test against the development URLs for the respective workflows, define constants for:

- `AUDIOEYE_API_URL`
- `AUDIOEYE_ACCOUNT_URL`

...in your `sites/default/settings.php` (or similar) or anywhere prior to `audioeye.module` loading.

Example:

In `settings.php`:

```
define('AUDIOEYE_API_URL', 'https://example.com/api');
define('AUDIOEYE_ACCOUNT_URL', 'https://example.com/account');
```
