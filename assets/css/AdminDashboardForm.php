<?php

/**  
 * @file  
 * Contains Drupal\audioeye\Form\AdminSettingsForm.  
 */

namespace Drupal\audioeye\Form;

use Drupal;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Exception;

class AdminDashboardForm extends ConfigFormBase
{

    const API_URL       = 'https://cmsapi.audioeye.com';
    const API_INSTALL   = 'drupal/install';
    const API_SIGNATURE = 'drupal/signature';

    /**  
     * {@inheritdoc}  
     */
    protected function getEditableConfigNames()
    {
        return [
            'audioeye.admin_settings',
        ];
    }

    /**  
     * {@inheritdoc}  
     */
    public function getFormId()
    {
        return 'audioeye_admin_dashboard';
    }

    /**  
     * {@inheritdoc}  
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('audioeye.admin_settings');
        // dump($config);
        // dd($config);

        // $form['admin_settings'] = [
        //     '#type' => 'textarea',
        //     '#title' => $this->t('Welcome message'),
        //     '#description' => $this->t('Welcome message display to users when they login'),
        //     '#default_value' => $config->get('welcome_message'),
        // ];

        $app = Drupal::getContainer()->get('site.path');

        $host = \Drupal::request()->getHost();
        $host2 = \Drupal::request()->getSchemeAndHttpHost();
        // dd(\Drupal::config('system.site'), $host, $config);


        $form['audioeye_link_dashboard'] = [
            '#type'     => 'inline_template',
            '#template' => '<div>Navigate to &raquo; <a style="{{style}}" href="{{url}}">Settings</a> <hr></div>',
            '#context'  => [
                'url' => Url::fromRoute('audioeye.admin_settings'),
                'style' => 'display: inline-block; margin: 15px 0',
            ],
        ];


        $form['audioeye_admin_iframe'] = [
            '#type'     => 'inline_template',
            '#template' => '<iframe onload="__AudioEyeDrupal.resize(\'#audioeye-dashboard\')" frameborder="0" id="audioeye-dashboard" width="100%" src="{{url|raw}}"></iframe>',
            '#context'  => [
                'url' => sprintf(
                    'https://portal.audioeye.com/login/channel?ext_id=%s&sig=%s&ts=%s&channel_name=drupal',
                    $config->get('ext_id'),
                    $config->get('signature'),
                    $config->get('timestamp')
                ),
            ],
        ];


        $form['audioeye_admin_iframex'] = [
            '#type'     => 'inline_template',
            '#template' => "<script>function resizeIframe(obj) { console.log(obj) /* obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px'; */}</script>",
            // '#context'  => [
            //     'url' => sprintf(
            //         'https://portal.audioeye.com/login/channel?ext_id=%s&sig=%s&ts=%s&channel_name=drupal',
            //         $config->get('ext_id'),
            //         $config->get('signature'),
            //         $config->get('timestamp')
            //     ),
            // ],
        ];

        $form = parent::buildForm($form, $form_state);

        $form['actions'] = '';
        // $form['actions']['submit']['#value'] = $this->t('Save Settings');

        return $form;
    }


    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        // Validation is optional.

        $email = $form_state->getValue('settings_app_email');

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $form_state->setErrorByName('admin_settings_app_email', 'Invalid email address, please try again.');
        }

        // dump($form_state->getTriggeringElement());
        // dd($form_state, $form_state->getValue('admin_settings_app_email'), filter_var($email, FILTER_VALIDATE_EMAIL));
    }

    /**  
     * {@inheritdoc}  
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitForm($form, $form_state);

        $email = $form_state->getValue('settings_app_email');

        $extension = $this->getExtensionDetails($email);

        // dd($extension);

        $this->config('audioeye.admin_settings')
            ->set('email', $email)
            ->set('ext_id', $extension['ext_id'])
            ->set('site_hash', $extension['site_hash'])
            ->set('timestamp', $extension['timestamp'])
            ->set('signature', $extension['signature'])
            ->save();
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    protected function getExtensionDetails(string $email)
    {
        // "site2@audioeye.test"
        $request   = $this->getApiExtensionId($email);
        $signature = $this->getApiSignature($request['ext_id']);

        return array_merge($request, $signature);
        // dd(__METHOD__, $request);
        // dd(__METHOD__, $request, $signature, array_merge($request, $signature));
    }

    /**
     * Undocumented function
     *
     * @param string $endpoint
     *
     * @return void
     */
    protected function getApiUrl(string $endpoint = '')
    {
        return trim(self::API_URL, "\/") . '/' . $endpoint;
    }

    /**
     * Undocumented function
     *
     * @param string $endpoint
     *
     * @return void
     */
    protected function getApiExtensionId(string $email)
    {
        return $this->doApiRequest('POST', self::API_INSTALL, [
            'headers' => [
                'Accept' => 'application/json',
            ],
            'form_params' => [
                "email"    => $email,
                "base_url" => \Drupal::request()->getSchemeAndHttpHost()
            ],
        ]);
    }

    /**
     * Undocumented function
     *
     * @param string $ext_id
     *
     * @return void
     */
    protected function getApiSignature(string $ext_id)
    {
        return $this->doApiRequest('GET', self::API_SIGNATURE, [
            'headers' => [
                'Accept' => 'application/json',
            ],
            'query' => [
                "ext_id" => $ext_id
            ],
        ]);
    }

    /**
     * Undocumented function
     *
     * @param string $method
     * @param string $endpoint
     * @param array $options
     *
     * @return void
     */
    protected function doApiRequest(string $method, string $endpoint = '', array $options = [])
    {

        try {
            /** @var GuzzleHttp\Client $client */
            $client = \Drupal::httpClient();

            $request = $client->request(
                $method,
                $this->getApiUrl($endpoint),
                $options
            );

            $response = $request->getBody()->getContents();

            if (is_string($response)) {
                $response = json_decode($response, JSON_OBJECT_AS_ARRAY);
            }

            return $response;
        } catch (\Exception $error) {

            $prefix = 'AUDIOEYE_ERROR_BEGIN::' . __FUNCTION__;
            $suffix = 'AUDIOEYE_ERROR_END::'  . __FUNCTION__;

            $message = [
                $prefix,
                "ERROR:\n\n {$error->getMessage()}",
                "TRACE:\n\n {$error->getTraceAsString()}",
                $suffix,
                str_repeat('#', 80)
            ];

            error_log(implode("\n\n", $message));

            \Drupal::logger('audioeye')->error(implode("\n", $message));
        }

        // if null we have a problem, check the logs
        // for debug info and any exceptions.
        return null;
    }
}
