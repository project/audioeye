! function ($) {
  window.__AudioEyeDrupal = {
    resize: function (target) {
        var page    = $(".page-content").css('margin-bottom');
        var height  = $(window).height() - $(target).offset().top - parseInt(page, 10);
        $(target).css('height', height + 'px')
    }
  }
}(jQuery);